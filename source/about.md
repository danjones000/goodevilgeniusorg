---
id: 5
title: About
date: 2008-04-26T17:07:00+00:00
author: Danjones
layout: page
guid: http://goodevilgenius.org/wordpress/?page_id=2
tags:
    - me
    - introduction
---
My name is Dan Jones. I'm a [Software Engineer](https://danielrayjones.com/web/) specializing in web development using PHP and golang stacks. I live in the Houston area. I'm also a father and an actor.
